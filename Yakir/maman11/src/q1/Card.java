//Yakir Yehezkel ID. 066513052
package q1;

public class Card {
    private String face; // face of card ("Ace", "Deuce", ...)
    private String suit; // suit of card ("Hearts", "Diamonds", ...)

    // two-argument constructor initializes card's face and suit
    public Card(String cardFace, String cardSuit){
        face = cardFace;
        suit = cardSuit;
    }

    // return String representation of Card
    public String toString(){
        return face + " of " + suit;
    }
}
