//Yakir Yehezkel ID. 066513052
package q1;

import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

import java.util.ArrayList;
import java.util.Random;
import java.util.Arrays;

public class DeckOfCards {
    private ArrayList<Card> deckDynamic;
    private int currentCard;
    private Random randomNumbers;
    private final int NUMBER_OF_CARDS = 52;
    private final String faces[] = {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven",
            "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
    private final String suits[] = {"Hearts", "Diamonds", "Clubs", "Spades"};

    // constructor fills deck of Cards
    public DeckOfCards() {
        deckDynamic = new ArrayList<Card>(NUMBER_OF_CARDS);
        currentCard = 0;
        randomNumbers = new Random();

        for (int count = 0; count < NUMBER_OF_CARDS; count++) {
            deckDynamic.add(new Card(faces[count % 13], suits[count / 13]));
        }
    }

    // shuffle deck of Cards with one-pass algorithm
    public void shuffle() {
        currentCard = 0;
        for (int first = 0; first < deckDynamic.size(); first++) {
            int second = randomNumbers.nextInt(52);
            Card temp = deckDynamic.get(first);
            deckDynamic.set(first, deckDynamic.get(second));
            deckDynamic.set(second, temp);
        }
    }

    //Deal card and increment to the next one.
    public Card dealCard() {
        if (currentCard < deckDynamic.size())
            return deckDynamic.get(currentCard++);
        else
            return null;
    }

    public void createEmptyDeck() {
        deckDynamic.clear();
    }

    public void addCardToDeck(Card newCard) {
        deckDynamic.add(newCard);
    }

    // Get the integer reference of the card face
    public int getFace(){
        String valueString = deckDynamic.get(0).toString();
        int cardValue = Arrays.asList(faces).indexOf(valueString.split(" of ")[0]) + 1;
        return cardValue;
    }

    //Remove card from deck - use to substitute card from player deck
    public void removeCard() {
        deckDynamic.remove(0);
    }

    //Get card from player deck.
    // return null when deck is empty
    public Card getCard() {
        try {
            return deckDynamic.get(0);
        }
        catch (Exception e) {
            System.out.println("No card to get left!");
            return null;
        }
    }

    //Add list of cards - use to add number of cards
    public void addListOfCards(ArrayList <Card> arrayToAdd) {
        deckDynamic.addAll(deckDynamic.size(), arrayToAdd);
    }

    //Check the player deck size
    public int getArraySize() {
        return deckDynamic.size();
    }
}
