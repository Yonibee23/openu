//Yakir Yehezkel ID. 066513052
package q1;

public class RunGameTest {

    public static void main(String args[]) {
        String turnWinner;
        String warWinner;
        boolean gameOver = false;
        GameProcess runGame = new GameProcess();
        runGame.DealCardsToPlayers(); //Deal cards to both players

        while (!gameOver){
            gameOver = runGame.checkWinnerOfGame();
            if(gameOver){return;}
            runGame.GameTurn(); //play regular turn
            turnWinner = runGame.CheckWinner(); //check winner on regular turn
            if (turnWinner == null) {
                do {
                    gameOver = runGame.checkWinnerOfGame(); //check winner of the game
                    if(gameOver){return;}
                    runGame.WarTurn(); //play war turn
                    gameOver = runGame.checkWinnerOfGame();
                    if(gameOver){return;}
                    warWinner = runGame.CheckWinner();
                } while (warWinner == null && !gameOver);

            }
        }
    }
}
