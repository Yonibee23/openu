//Yakir Yehezkel ID. 066513052
package q1;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import java.util.ArrayList;

public class GameProcess {
    private DeckOfCards myDeckOfCards = new DeckOfCards();;
    private DeckOfCards player1Deck = new DeckOfCards();
    private DeckOfCards player2Deck = new DeckOfCards();
    private Card card1;
    private Card card2;
    private ArrayList<Card> tempDeck = new ArrayList<Card>();

    //Constructor that shuffle the main deck of cards and create empth deck for both players
    public GameProcess(){
        myDeckOfCards.shuffle();
        player1Deck.createEmptyDeck();
        player2Deck.createEmptyDeck();
    }

    //Deal cards to both players
    public void DealCardsToPlayers(){
        for(int j = 0; j < 26; j++){
            player1Deck.addCardToDeck(myDeckOfCards.dealCard());
            player2Deck.addCardToDeck(myDeckOfCards.dealCard());
        }
    }

    //declare the winner by by checking the other player deck size
    public boolean checkWinnerOfGame(){
        if(player1Deck.getArraySize() < 1) {
            JOptionPane.showMessageDialog(null, "Player-2 won the game!!!");
            return true;
        }
        else if(player2Deck.getArraySize() < 1) {
            JOptionPane.showMessageDialog(null, "Player-1 won the game!!!");
            return true;
        }
        else
            return false;
    }

    //Play a turn - get card from each player, add those cards to temporary deck
    public void GameTurn(){
        card1 = player1Deck.getCard();
        card2 = player2Deck.getCard();
        tempDeck.clear();
        tempDeck.add(card1);
        tempDeck.add(card2);

    }

    //Check player winner by comparing the decimal reference of the face.
    // remove cards from both players than add cards list to the player who won.
    public String CheckWinner(){
        if (player1Deck.getFace() > player2Deck.getFace()){
            JOptionPane.showMessageDialog(null, String.format("Player-1: %-20s Player-2: %-20s\nPlayer 1 win!\n", card1, card2));
            player1Deck.removeCard();
            player2Deck.removeCard();
            player1Deck.addListOfCards(tempDeck);
            return "Player 1 win!";
        }
        else if (player1Deck.getFace() < player2Deck.getFace()){
            JOptionPane.showMessageDialog(null, String.format("Player-1: %-20s Player-2: %-20s\nPlayer 2 win!\n", card1, card2));
            player1Deck.removeCard();
            player2Deck.removeCard();
            player2Deck.addListOfCards(tempDeck);
//            System.out.println("Player 2 win!\n");
            return "Player 2 win!";
        }
        else
            return null;
    }

    //Play war turn - play 'regular' war turn in case both players deck > 3,
    // else the number of war turns is the minimum size of the players decks
    public void WarTurn(){
        int loopIndex = 0;
        if((player1Deck.getArraySize() > 3) && (player2Deck.getArraySize() > 3))
            loopIndex = 3;
        else
            loopIndex = Math.min(player1Deck.getArraySize(), player2Deck.getArraySize());
        for(int i = 0; i < loopIndex; i++){
            JOptionPane.showMessageDialog(null, String.format("WAR!\nPlayer-1: %-20s Player-2: %-20s\n", card1, card2));

            tempDeck.add(card1);
            tempDeck.add(card2);
            player1Deck.removeCard();
            player2Deck.removeCard();
            card1 = player1Deck.getCard();
            card2 = player2Deck.getCard();
        }
    }
}
