//Yakir Yehezkel ID. 066513052
package q2;

public class Cell {
    private int lifeOrDeath; //Parameter of cell value: '1' = life, '0' = death
    private int xCoordinate; //Cell coordinate x on the matrix
    private int yCoordinate; //Cell coordinate x on the matrix
    private int[] getCellCoordinate = new int[2];

    //Constructor that check if the cell value is greater than '1' or less than '0'.
    public Cell(int lifeOrDeath, int xCoordinate, int yCoordinate){
        if(lifeOrDeath < 0 || lifeOrDeath > 1)
            throw new IllegalArgumentException(String.format("Cell value that define 'life' or 'death' (%d) must be 0/1", lifeOrDeath));
        this.lifeOrDeath = lifeOrDeath; //life = '1', death = '0'
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
    }

    // return int representation of Cell (1=life, 0=death)
    public int getCellValue(){
        return lifeOrDeath;
    }

    // set new value to Cell
    public void setCellValue(int newCellValue){
        lifeOrDeath = newCellValue;
    }

    // Check if Cell not exceed from matrix range.
    public boolean checkCellValid(int row, int col){
        return row >= 0 && col >= 0 && row <= 9 && col <= 9;
    }

    // Return Cell coordinate in array
    public int[] getCellCoordinate(){
        getCellCoordinate[0] = xCoordinate;
        getCellCoordinate[1] = yCoordinate;
        return getCellCoordinate;
    }

    // Return the string representation of a cell.
    public String toString(){
        return String.format("%d - (%d,%d)", lifeOrDeath, xCoordinate, yCoordinate);
    }
}
