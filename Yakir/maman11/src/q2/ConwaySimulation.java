//Yakir Yehezkel ID. 066513052
package q2;

import java.util.ArrayList;
import java.util.List;

public class ConwaySimulation {
    private LifeMatrix myLifeMatrix;
    private LifeMatrix nextLifeMatrix;
    private List<List<Integer>> myNeighbors;
    private Cell tempCell;
    private Cell currentCell;
    private int cellValue;
    private int[] neighborsValues = new int[2];
    private int[] cellCoordinate = new int[2];
    private Cell[][] temp_myLifeMatrix;
    private Cell[][] temp_nextLifeMatrix;

    //Constructor that fill the 'next generation' array with '0', and fill the current matrix with random values '0'/'1'
    public ConwaySimulation(final int MATRRIX_SIZE){
        myLifeMatrix = new LifeMatrix(MATRRIX_SIZE, MATRRIX_SIZE);
        nextLifeMatrix = new LifeMatrix(MATRRIX_SIZE, MATRRIX_SIZE);
        nextLifeMatrix.emptyMatrix();
        myLifeMatrix.arrangeMatrix();
        myLifeMatrix.printMatrix("First Matrix");
        tempCell = nextLifeMatrix.getCell();
        currentCell = myLifeMatrix.getCell();
    }

    //For each cell in current generation - get life counter from all neighbors
    public void currentGeneration() {
        myNeighbors = new ArrayList<>();
        myNeighbors.clear();

        currentCell = myLifeMatrix.nextCell();
        tempCell = nextLifeMatrix.nextCell();
        cellValue = currentCell.getCellValue();
        cellCoordinate = currentCell.getCellCoordinate();
        myNeighbors = myLifeMatrix.getRelevantNeighbors(cellCoordinate[0], cellCoordinate[1]);
        neighborsValues = myLifeMatrix.getNeighborsValues(myNeighbors, cellCoordinate);
    }

    // Set the cell value for next generation according to the life counter.
    public void deathLifeExistence(){
        if(cellValue == 0) {   //Start with death
            if(neighborsValues[0] == 3) {
                tempCell.setCellValue(1);
            }
            else
                tempCell.setCellValue(0);
        }
        else {                  //Start with life
            if(neighborsValues[0] <= 1 || neighborsValues[0] >= 4)
                tempCell.setCellValue(0);
            else
                tempCell.setCellValue(1);
        }
        // Add the new cell to the 'next generation' matrix
        nextLifeMatrix.setCellToMatrix(tempCell.getCellValue(), tempCell.getCellCoordinate()[0], tempCell.getCellCoordinate()[1]);
    }

    // Print the matrix as a checkerboard - '1' = black, '0' = white.
    public void drawTest() {
        myLifeMatrix.printMatrix("Next Generation Matrix");
    }

    //prepare for next generation:
    // 1. copy 'next generation' matrix to 'current generation'
    // 2. fill next generation matrix with '0'
    public void getNextGen(){
        temp_myLifeMatrix = myLifeMatrix.emptyMatrix();
        temp_nextLifeMatrix = nextLifeMatrix.getMatrix();
        myLifeMatrix.copyMatrix(temp_nextLifeMatrix, temp_myLifeMatrix);
        nextLifeMatrix.emptyMatrix();
        tempCell = nextLifeMatrix.getCell();
        currentCell = myLifeMatrix.getCell();
    }
}
