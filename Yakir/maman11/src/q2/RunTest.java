//Yakir Yehezkel ID. 066513052
package q2;

import javax.swing.*;

public class RunTest {
    public static void main(String args[]) {
        int userChoise;
        final int MATRRIX_SIZE = 10;

        ConwaySimulation runGame = new ConwaySimulation(MATRRIX_SIZE);
        do {
            userChoise = JOptionPane.showConfirmDialog(null, "Next generation?");
            switch (userChoise) {
                case 1:
                    return; //break the switch and while loop
                case 2:
                    return; //break the switch and while loop
                case -1:
                    System.exit(0);
            }
            for (int i = 0; i < (MATRRIX_SIZE*MATRRIX_SIZE-1); i++) {
                runGame.currentGeneration(); //Get information of the current cell
                runGame.deathLifeExistence(); //Get if next generation this cell will be dead/alive
            }
            runGame.getNextGen(); //create a matrix of the next generation
            runGame.drawTest(); //draw the next generation matrix
        } while (userChoise == 0) ;
    }
}
