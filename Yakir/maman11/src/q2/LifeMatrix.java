//Yakir Yehezkel ID. 066513052
package q2;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LifeMatrix {
    JFrame window = new JFrame();
    private Cell[][] matrixOfCell; //Two dimension array of type Cell - represent the life matrix
    private int neighborsValues[] = new int[2]; //Array that hold the count of life and death of relevant neighbors
    private Random randomCell;
    private int currentCellRow;
    private int currentCellCol;
    private int lifeCounter;
    private int deathCounter;
    private int rowMatrix;
    private int colMatrix;
    private int loopCounter = -1; //use to increment row in the matrix
    private List<List<Integer>> neighborsArrayLists = new ArrayList<>(); //Dynamic list of list for relevant neighbors
    // Two dimensional list of all possible neighbors.
    private int[][] neighborsArray = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};

    //Constructor of the lise matrix that takes the matrix size.
    public LifeMatrix(int rowMatrix, int colMatrix) {
        this.rowMatrix = rowMatrix;
        this.colMatrix = colMatrix;
        randomCell = new Random();
        matrixOfCell = new Cell[rowMatrix][colMatrix];
        currentCellRow = 0;
        currentCellCol = 0;
    }

    //Method that randomly fill the matrix with '1'/'0'
    public void arrangeMatrix() {
        currentCellRow = 0;
        currentCellCol = 0;
        for (int row = 0; row < matrixOfCell.length; row++) {
            for (int col = 0; col < matrixOfCell[row].length; col++) {
                int setCell = randomCell.nextInt(2);
                matrixOfCell[row][col] = new Cell(setCell, row, col);
            }
        }
    }

    //Method that return the matrix of type Cell
    public Cell[][] getMatrix() {
        return matrixOfCell;
    }

    //Empty the matrix
    public Cell[][] emptyMatrix() {
        currentCellRow = 0;
        currentCellCol = 0;
        for (int row = 0; row < matrixOfCell.length; row++) {
            for (int col = 0; col < matrixOfCell[row].length; col++) {
                matrixOfCell[row][col] = new Cell(0, row, col);
            }
        }
        loopCounter = 0;
        return matrixOfCell;
    }

    // Copy one matrix to other matrix - use to fill next generation matrix.
    public Cell[][] copyMatrix(Cell[][] fromMatrixOfCell, Cell[][]toMatrixCell) {
        currentCellRow = 0;
        currentCellCol = 0;
        for (int row = 0; row < fromMatrixOfCell.length; row++) {
            for (int col = 0; col < fromMatrixOfCell[row].length; col++) {
                toMatrixCell[row][col] = fromMatrixOfCell[row][col];
            }
        }
        loopCounter = 0;
        return toMatrixCell;
    }

    //Print the matrix as a checkerboard - '1' = black, '0' = white.
    public void printMatrix(String title) {

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setTitle(title);
        for (int row = 0; row < matrixOfCell.length; row++) {
            for (int col = 0; col < matrixOfCell[row].length; col++) {
                DrawChessBoard drawing = new DrawChessBoard(matrixOfCell[row][col].getCellValue(), row, col);
                window.add(drawing);
                drawing.repaint();
                window.setSize(500,500);
                window.setVisible(true);
            }
        }
    }

    //Set specific cell in matrix with value - use to fill temp matrix that hold all next generation changed values
    public void setCellToMatrix(int value, int row, int col) {
        matrixOfCell[row][col] = new Cell(value, row, col);
    }

    //Get through all cells in matrix.
    public Cell nextCell() {
        ++loopCounter; //use to increment row in the matrix
        if (currentCellCol < matrixOfCell.length) {
            return matrixOfCell[currentCellRow][currentCellCol++ % colMatrix];
        }
        else
            return matrixOfCell[loopCounter / rowMatrix][currentCellCol++ % colMatrix];

    }

    public Cell getCell() {
        return matrixOfCell[currentCellRow][currentCellCol];
    }

    //Dynamic list of list that return all relevant neighbors of specific cell
    public List<List<Integer>> getRelevantNeighbors(int cellRow, int cellCol) {
        neighborsArrayLists.clear();
        for (int[] element : neighborsArray) {
            List<Integer> currentNeighbor = new ArrayList<>();
            currentNeighbor.clear();
            Cell currentCell = matrixOfCell[cellRow][cellCol];

            if (currentCell.checkCellValid(cellRow + element[0], cellCol + element[1])) {
                for (int elementValue : element)
                    currentNeighbor.add(elementValue);
            }
            neighborsArrayLists.add(currentNeighbor);
        }
        return neighborsArrayLists;
    }

    //Count the life and death values from all relevant neighbors.
    public int[] getNeighborsValues(List<List<Integer>> neighborsList, int[] cellCoordinate) {
        int cellRow = 0;
        int cellCol = 0;
        lifeCounter = 0;
        deathCounter = 0;

        for (int listRow = 0; listRow < neighborsList.size(); listRow++) {
            if (neighborsList.get(listRow).isEmpty())
                continue;
            cellRow = neighborsList.get(listRow).get(0);
            cellCol = neighborsList.get(listRow).get(1);

            Cell neighborCell = matrixOfCell[cellCoordinate[0] + cellRow][cellCoordinate[1] + cellCol];

            if (neighborCell.getCellValue() == 1)
                lifeCounter += 1;
            else
                deathCounter += 1;

        }
        neighborsValues[0] = lifeCounter;
        neighborsValues[1] = deathCounter;
        return neighborsValues;
    }
}
