//Yakir Yehezkel ID. 066513052
package q2;

import javax.swing.JPanel;
import java.awt.*;

public class DrawChessBoard extends JPanel {
    int color;
    int col;
    int row;

    public DrawChessBoard(int color, int col, int row) {
        this.color = color;
        this.col = col;
        this.row = row;
    }

    public void paintComponent(Graphics g) {

        int x, y;   // Top-left corner of square
        x = row * 40;
        y = col * 40;

        if (this.color == 1)
            g.setColor(Color.black);
        else
            g.setColor(Color.white);
        g.drawRect(x, y, 40, 40);
        g.fillRect(x, y, 39, 39);

    }
}
