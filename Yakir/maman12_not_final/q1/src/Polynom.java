import java.util.ArrayList;
import java.util.Collections;

public class Polynom {
    private final ArrayList<Number> polArray = new ArrayList<Number>();

    //Constructor
    public Polynom(ArrayList<Double> varArray, ArrayList<Integer> exponentArray){
        //Validate that both exponent and variables arrays size are equal.
        if(varArray.size() != exponentArray.size())
            throw new IllegalArgumentException("The size of variable array and exponent array not equal!");

        for (int index = 0; index < varArray.size(); index++) {
            this.polArray.add(new Number(varArray.get(index), exponentArray.get(index)));
        }
        sortPolynom(); //Call private method to sort the polynom
    }

    //Constructor that get polynom and copy it content
    public Polynom(Polynom p){
        polArray.addAll(p.polArray);
    }

    //Private provided that the method does not directly/indirectly call an overridable method.
    //Execute 'bubble sort' method to sort the polynom
    private void sortPolynom() {
        for (int index = 0; index < polArray.size()-1; index++) {
            for (int index2 = 0; index2 < polArray.size()-1; index2++) {
                if (polArray.get(index2).getExponentNumber() < polArray.get(index2 + 1).getExponentNumber())
                    Collections.swap(polArray, index2, index2+1);
            }
        }
    }

    //Plus method that sum the rational numbers with the same exponent, else leave the variable as is.
    final Polynom plus(Polynom userPol){
        Polynom res = new Polynom(this);
        Polynom copyPol = new Polynom(userPol);
        Number thisNumber;
        Number thatNumber;

        for (int index = 0; index < res.polArray.size(); index++) {
            thisNumber = res.polArray.get(index);
            for(int index2 = 0; index2 < copyPol.polArray.size(); index2++) {
                thatNumber = copyPol.polArray.get(index2);

                if (thisNumber.getExponentNumber() == thatNumber.getExponentNumber()) {
                    res.polArray.set(index, new Number(thisNumber.getRationalNumber() +
                            thatNumber.getRationalNumber(), thisNumber.getExponentNumber()));
                    copyPol.polArray.remove(index2);
                    break;
                }
            }
        }
        res.polArray.addAll(copyPol.polArray);
        res.sortPolynom();
        return res;
    }

    //Minus method that subtitue the rational numbers with the same exponent,
    //else multiply by '-1' all variables after the '-' sign.
    final Polynom minus(Polynom userPol){
        Polynom res;
        Polynom copyPol = new Polynom(userPol);
        Number thisNumber;

        for(int index = 0; index < copyPol.polArray.size(); index++) {
            thisNumber = copyPol.polArray.get(index);
            //Multiply all variables of the second polynom by '-1', then call the 'plus' method.
            copyPol.polArray.set(index, new Number(thisNumber.getRationalNumber()*-1, thisNumber.getExponentNumber()));
        }

        res = this.plus(copyPol);
        return res;
    }

    //Multiply rational number with the exponent number,
    //then decrease exponent by '1'.
    final Polynom derivative(Polynom userPol){
        Polynom res = new Polynom(userPol);
        Number thatNumber;

        for(int index = 0; index < res.polArray.size(); index++) {
            thatNumber = res.polArray.get(index);
            if(thatNumber.getExponentNumber() == 0)
                res.polArray.set(index, new Number(0.0, 0));
            else {
                res.polArray.set(index, new Number(thatNumber.getRationalNumber() * thatNumber.getExponentNumber(),
                        thatNumber.getExponentNumber() -1));
            }
        }
        return res;
    }

    //Override method equals from Object.
    //Check that all variables in both arrays are equal by rational numbers and exponent.
    @Override
    public boolean equals(Object obj) {
        // Check if obj is an instance of Polynom or not
        if(!(obj instanceof Polynom))
            return false;
        // typecast obj to Polynom so that we can compare data members
        Polynom q = (Polynom) obj;
        if(this.polArray.size() != q.polArray.size())
            return false;

        Number thisNumber;
        Number thatNumber;
        for(int index = 0; index < this.polArray.size(); index++) {
            thisNumber = this.polArray.get(index);
            thatNumber = q.polArray.get(index);
            if(thisNumber.getExponentNumber() != thatNumber.getExponentNumber())
                return false;
            else if(thisNumber.getRationalNumber() != thatNumber.getRationalNumber())
                return false;
        }
        return true;
    }

    //Override toString method.
    //Use toString method from Number class
    @Override
    public String toString() {
        StringBuilder newString = new StringBuilder();
        for (int index = 0; index < polArray.size(); index++) {
            newString.append(polArray.get(index).toString());
            if(index < polArray.size()-1){
                if(polArray.get(index+1).getRationalNumber()>=0)
                    newString.append(" + ");
                else
                    newString.append(" ");
            }
        }
        return newString.toString();
    }
}
