public class Number {
    private double rationalNumber;
    private int exponentNumber;

    //Constructor when call with empty arguments - set default values
    public Number(){
        this(0.0, 0);
    }

    //Constructor when call with only rational number argument - set exponent to default value
    public Number(double rationalNumber){
        this(rationalNumber, 0);
    }

    //Constructor when call with only exponent number argument - set rational number to default value
    public Number(int exponentNumber){
        this(0.0, exponentNumber);
    }

    //Constructor with both rational and exponent values.
    public Number(double rationalNumber, int exponentNumber){
        //Check if exponent < 0, throw exception if yes.
        if(exponentNumber < 0)
            throw new IllegalArgumentException("Exponent must be positive");

        this.rationalNumber = rationalNumber;
        this.exponentNumber = exponentNumber;
    }

    public double getRationalNumber() {
        return rationalNumber;
    }

    public int getExponentNumber() {
        return exponentNumber;
    }

    public void setExponentNumber(int exponentNumber) {
        if(exponentNumber < 0)
            throw new IllegalArgumentException("Exponent must be positive");
        this.exponentNumber = exponentNumber;
    }

    public void setRationalNumber(double rationalNumber) {
        this.rationalNumber = rationalNumber;
    }

    //Override toString method.
    @Override
    public String toString() {
        if (rationalNumber == 0)
            return "0";
        else {
            switch (exponentNumber) {
                case 0:
                    return String.format("%.1f", rationalNumber);
                case 1:
                    if (rationalNumber == 1.0)
                        return "x";
                    else if (rationalNumber == -1)
                        return "-x";
                    else
                        return String.format("%.1fx", rationalNumber);
                default:
                    if (rationalNumber == 1.0)
                        return String.format("x^%d", exponentNumber);
                    else if (rationalNumber == -1.0)
                        return String.format("-x^%d", exponentNumber);
                    else
                        return String.format("%.1fx^%d", rationalNumber, exponentNumber);
            }
        }
    }
}
