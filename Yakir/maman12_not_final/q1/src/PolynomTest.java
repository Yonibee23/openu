import java.util.ArrayList;
import java.util.Scanner;

public class PolynomTest {

    public static void main(String[] args) {

        //first polynom contain two array:
        //1 - double for rational number. 2 - int for exponent
        ArrayList<Integer> myExponent;
        ArrayList<Double> myvariable;

        //second polynom contain two array:
        //1 - double for rational number. 2 - int for exponent
        ArrayList<Integer> myExponent2;
        ArrayList<Double> myvariable2;

        //Fill both variables and exponent arrays for both polynoms.
        myvariable = fillVariableArray();
        myExponent = fillExponentArray();
        Polynom myPolynom = new Polynom(myvariable, myExponent);

        myvariable2 = fillVariableArray();
        myExponent2 = fillExponentArray();
        Polynom mySecondPolynom = new Polynom(myvariable2, myExponent2);

        //Results polynom.
        Polynom rPolynom;

        //Call the 'plus' operation
        System.out.print(String.format("p + q = (%s) + (%s)\n", myPolynom.toString(), mySecondPolynom.toString()));
        rPolynom = myPolynom.plus(mySecondPolynom);
        System.out.print(String.format("= %s", rPolynom.toString()));

        //Call the 'minus' operation
        System.out.print(String.format("\n\np - q = (%s) - (%s)\n", myPolynom.toString(), mySecondPolynom.toString()));
        rPolynom = myPolynom.minus(mySecondPolynom);
        System.out.print(String.format("= %s", rPolynom.toString()));

        //Call the 'derivative' operation
        System.out.print(String.format("\n\nq' = (%s)'\n", mySecondPolynom.toString()));
        rPolynom = myPolynom.derivative(mySecondPolynom);
        System.out.print(String.format("= %s", rPolynom.toString()));

        //Call the override equals operation.
        if (myPolynom.equals(mySecondPolynom))
            System.out.println("\n\nPolynoms are equal ");
        else
            System.out.println("\n\nPolynoms are not equal ");
    }

    //Method to fill ArrayList of double from user
    private static ArrayList<Double> fillVariableArray(){
        ArrayList<Double> variable = new ArrayList<>();
        System.out.println("Enter the polynom variables.\nTo exit send 'q'");
        Scanner sc = new Scanner(System.in);
        while (!sc.hasNext("q")) {
            if (sc.hasNextDouble()) {
                double var = sc.nextDouble();
                variable.add(var);
            } else
                sc.next();
        }
        return variable;
    }

    //Method to fill ArrayList of integer from user
    private static ArrayList<Integer> fillExponentArray(){
        ArrayList<Integer> Exponent = new ArrayList<>();
        System.out.println("Enter the polynom exponent.\nTo exit send 'q'");
        Scanner sc = new Scanner(System.in);
        while (!sc.hasNext("q")) {
            if (sc.hasNextInt()) {
                int exp = sc.nextInt();
                Exponent.add(exp);
            } else
                sc.next();
        }
        return Exponent;
    }
}
