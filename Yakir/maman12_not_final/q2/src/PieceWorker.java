public class PieceWorker extends Employee {
    private double salaryPerItem;
    private int itemsQty;


    //constructor
    public PieceWorker(String firstName, String lastName, String socialSecurityNumber, BirthDate birthDate, double salaryPerItem, int itemsQty, double bonus){
        //constructor for abstract class who can't get 'new'
        super(firstName, lastName, socialSecurityNumber, birthDate);
        if (salaryPerItem <= 0.0)
            throw new IllegalArgumentException("Salary per item must be > 0.0");
        if (itemsQty < 0.0)
            throw new IllegalArgumentException("items quantity must be >= 0.0");
        if (bonus < 0.0)
            throw new IllegalArgumentException("bonus must be >= 0.0");
        this.salaryPerItem = salaryPerItem;
        this.itemsQty = itemsQty;
    }

    //set items quantity
    public void setItemsQty(int itemsQty) {
        this.itemsQty = itemsQty;
    }

    //return items quantity
    public int getItemsQty() {
        return itemsQty;
    }

    //set salary per item
    public void setSalaryPerItem(double salaryPerItem) {
        this.salaryPerItem = salaryPerItem;
    }

    //return salary per item
    public double getSalaryPerItem() {
        return salaryPerItem;
    }

    //calculate earnings: override abstract method earnings in Employee
    @Override
    public double earnings(){
        return getItemsQty() * getSalaryPerItem();
    }

    //return String representation of CommissionEmployee object
    @Override
    public String toString(){
        return String.format("%s: %s%n%s: %,.2f ILS; %s: %d", "Piece worker employee",
                super.toString(), "Salary per item", getSalaryPerItem(), "Items quantity", getItemsQty());
    }
}
