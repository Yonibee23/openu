public class BirthDate {
    private int month; // 1-12
    private int day; //1-31 based on month
    private int year;

    private static final int[] daysPerMonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    //empty constructor
    public BirthDate() {
        this(1, 1, 1);
    }
    //constructor
    public BirthDate(int day, int month, int year){
        //check if month in range
        if (month <= 0 || month > 12)
            throw new IllegalArgumentException("month (" + month + ") must be 1-12");
        //check if dayes in range for month
        if (day <= 0 || (day > daysPerMonth[month] && !(month == 2 && day == 29)))
            throw new IllegalArgumentException("day (" + day + ") out of range for the specified month and year");
        //check for leap year if month is 2 and day is 29
        if (month == 2 && day == 29 && !(year % 400 == 0 || year % 4 == 0 && year % 100 != 0))
            throw new IllegalArgumentException("day (" + day + ") out of range for the specified month and year");

        this.month = month;
        this.day = day;
        this.year = year;
    }
    //override equal - the method return true if the day and month are equals.
    @Override
    public boolean equals(Object obj){
        if (!(obj instanceof BirthDate))
            return false;
        BirthDate date = (BirthDate) obj;
        if (day != date.day || month != date.month)
            return false;
        return true;
    }

    //return a String representation of BirthDate
    @Override
    public String toString(){
        return String.format("%d/%d/%d", day, month, year);
    }
}
