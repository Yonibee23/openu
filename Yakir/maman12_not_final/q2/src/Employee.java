public abstract class Employee {
    private final String firstName;
    private final String lastName;
    private final String socialSecurityNumber;
    private final BirthDate birthDate;
    private final double bonus = 200.0; //bonus for birthday

    //constructor
    public Employee(String firstName, String lastName, String socialSecurityNumber, BirthDate birthDate){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
        this.birthDate = birthDate;
    }

    //return first name
    public String getFirstName(){
        return firstName;
    }

    //return last name
    public String getLastName(){
        return lastName;
    }

    //return social security number
    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }

    public BirthDate getBirthDate(){
        return birthDate;
    }

    //return bonus
    public double getBonus(){
        return bonus;
    }

    //return bonus for birthday - 200 + earnings
    public double earningPlusBonus(){
        return this.earnings() + getBonus();
    }

    //return String representation of employee object
    @Override
    public String toString(){
        return String.format("%s %s%nsocial security number: %s%nBirthday: %s", getFirstName(), getLastName(), getSocialSecurityNumber(), getBirthDate());
    }

    //abstract method must be overriden by concrete subclasses
    public abstract double earnings();
}
