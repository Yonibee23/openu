import java.awt.*;
import java.util.*;

public class PayrollSystemTest {
    private static ArrayList<Employee> employees = new ArrayList<>();
    private static Calendar calndr;
    private static BirthDate today;

    public static void main(String args[]) {
        calndr = Calendar.getInstance();

        // create subclass objects
        SalariedEmployee salariedEmployee = new SalariedEmployee("John", "Smith",
                "111-11-1111", new BirthDate(29, 11, 1987), 800.00);
        HourlyEmployee hourlyEmployee = new HourlyEmployee("Karen", "Price",
                "222-22-2222", new BirthDate(29, 11, 1988), 16.75, 40, 0);
        CommissionEmployee commissionEmployee = new CommissionEmployee("Sue", "Jones",
                "333-33-3333", new BirthDate(29, 11, 1984), 10000, .06);
        BasePlusCommissionEmployee basePlusCommissionEmployee = new BasePlusCommissionEmployee("Bob",
                "Lewis", "444-44-4444", new BirthDate(27, 10, 1990), 5000, .04, 300);
        PieceWorker pieceWorker = new PieceWorker("David",
                "Smith", "555-55-5555", new BirthDate(29, 11, 1979), 120.5, 3, 0);


        //initialize array list with Employee
        employees.add(salariedEmployee);
        employees.add(hourlyEmployee);
        employees.add(commissionEmployee);
        employees.add(basePlusCommissionEmployee);
        employees.add(pieceWorker);

        today = new BirthDate(calndr.get(calndr.DAY_OF_MONTH), calndr.get(calndr.MONTH) + 1, calndr.get(calndr.YEAR));

        PrintAllEmployees();
    }

    private static void PrintAllEmployees() {
        System.out.printf("\n\nEmployees processed polymorphically: %n%n");
        //generically process each element in array employees
        for (Employee currentEmployee : employees) {
            System.out.println(currentEmployee);
            if (today.equals(currentEmployee.getBirthDate())) {
                System.out.printf("Happy Birthday! Salary with 200 ILS increase is: %,.2f ILS%n%n", currentEmployee.earningPlusBonus());
                }
            else
                System.out.printf("earned %,.2f ILS%n%n", currentEmployee.earnings());
        }
    }
}
