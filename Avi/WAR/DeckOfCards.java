package com.company;
/**
 *
 * @author avieren
 */
// DeckOfCards class represents a deck of playing cards.
import java.util.ArrayList;
import java.util.Random;

public class DeckOfCards {

    private ArrayList<Card> deck; // array of Card objects
    private int currentCard; // index of next Card to be dealt
    //private final int NUMBER_OF_CARDS = 52; // constant number of Cards
    private Random randomNumbers; // random number generator

    // constructor fills deck of Cards
    public DeckOfCards(int numberOfCard) {
        //String faces[] = {"Ace", "Deuce", "Three", "Four", "Five", "Six",
        //                "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
        String suits[] = {"Hearts", "Diamonds", "Clubs", "Spades"};
        deck = new ArrayList<Card>(numberOfCard); // create dynamic array of Card objects
        currentCard = 0; // set currentCard so first Card dealt is deck[ 0 ]
        randomNumbers = new Random();
        for ( int count = 0; count < numberOfCard; count++ ){
            deck.add(count, new Card( Faces.values()[ count % 13 ], suits[ count / 13 ] ));
// create random number generator
        }
    } // end DeckOfCards first constructor
    //second constructor foremptey deck
    public DeckOfCards() {
        deck = new ArrayList<Card>(); // create dynamic array of Card objects
        currentCard = 0; // set currentCard so first Card dealt is deck[ 0 ]
    } // end DeckOfCards second constructor

    // shuffle deck of Cards with one-pass algorithm
    public void shuffle() {
        // after shuffling, dealing should start at deck[ 0 ] again
        currentCard = 0; // reinitialize currentCard

        // for each Card, pick another random Card and swap them
        for (int first = 0; first < (deck.size()-1); first++) {
            // select a random number between 0 and 51
            int second = randomNumbers.nextInt(deck.size()-1);
            // swap current Card with randomly selected Card
            if (first == second)
                continue;
            Card temp = deck.get(first);
            deck.remove(first);
            deck.add(first, deck.get(second));
            deck.remove(second);
            deck.add(second,temp);
        } // end for
    } // end method shuffle

    // deal one Card
    public Card dealCard() {
        // determine whether Cards remain to be dealt
        if (deck==null | deck.isEmpty()) {
            return null; // return null to indicate that all Cards were dealt
        } else {
            try{
            return deck.remove(0); // return current Card in array
        }
        catch ( java.lang.StackOverflowError stackOverflowError)
        {
            return null;
        }}
    } // end method dealCard

    public int getSize(){
        return deck.size();
    }
    public void addCard(Card card){
        deck.add(card);
    }
    public ArrayList getDeck(){
        return deck;
    }

    public void addDeck(ArrayList deckToAdd){
        deck.addAll(deckToAdd);
    }

    public boolean checkStatus() {
        return !deck.isEmpty();
    }
} // end class DeckOfCards