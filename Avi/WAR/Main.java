/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author avieren
 */
public class Main {

    private ArrayList<Card> war_p;
    private DeckOfCards dealer;
    private DeckOfCards p1_deck;
    private DeckOfCards p2_deck;
    private final int NUMBER_OF_CARDS = 52;

    public Main() {
        dealer = new DeckOfCards(NUMBER_OF_CARDS);
        dealer.shuffle();
        p1_deck = new DeckOfCards();
        p2_deck = new DeckOfCards();
        war_p = new ArrayList<Card>(4);
        for (int cnt = 0; cnt < NUMBER_OF_CARDS; cnt = cnt + 2) {
            p1_deck.addCard(dealer.dealCard());
            p2_deck.addCard(dealer.dealCard());
        }
    }

    private void play_milhama() {
        for (int i = 0; i < 2; i++) {
            //if ((p1_deck.checkStatus())&&(p2_deck.checkStatus())){
            war_p.add(p1_deck.dealCard());
            war_p.add(p2_deck.dealCard());
        }
       // }
        play();

   }

    private void play() {
        if ((p1_deck.checkStatus())&&(p2_deck.checkStatus())) {
            Card p1_card = p1_deck.dealCard();
            Card p2_card = p2_deck.dealCard();
           // if ((p1_card==null)|(p2_card==null))
              //  System.out.println("null");
            if (p1_card.get_value() > p2_card.get_value()) {
                p1_deck.addCard(p1_card);
                p1_deck.addCard(p2_card);
                if (!war_p.isEmpty()) {
                    p1_deck.addDeck(war_p);
                    war_p.clear();
                }
                //JOptionPane.showMessageDialog(null, String.format("p1 card: %d p2 card: %d%n the round winner is p1",p1_card.get_value(), p2_card.get_value()));
                System.out.println(String.format("p1 card: %d p2 card: %d%n the round winner is p1", p1_card.get_value(), p2_card.get_value()));
                play();
            } else if (p2_card.get_value() > p1_card.get_value()) {
                p2_deck.addCard(p1_card);
                p2_deck.addCard(p2_card);
                if (!war_p.isEmpty()) {
                    p2_deck.addDeck(war_p);
                    war_p.clear();
                }
                //JOptionPane.showMessageDialog(null, String.format("p1 card: %d p2 card: %d%n the round winner is p2",p1_card.get_value(), p2_card.get_value()));
                System.out.println(String.format("p1 card: %d p2 card: %d%n the round winner is p2", p1_card.get_value(), p2_card.get_value()));
                play();
            } else {
                play_milhama();
                //JOptionPane.showMessageDialog(null, "playing milhama");
            }
        }
    }

    public static void main(String[] args) {
        Main game = new Main();
        game.play();

        if (game.p1_deck.checkStatus()) {
            JOptionPane.showMessageDialog(null, "THE WINNER IS P1");
            System.out.println("p1");
        } else {
            JOptionPane.showMessageDialog(null, "THE WINNER IS P2");
            System.out.println("p2");
        }

    }
}
  