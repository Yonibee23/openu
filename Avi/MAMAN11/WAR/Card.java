package com.company;


/**
 *
 * @author avieren
 */
enum Faces {
    ACE(13),DEUCE(1),THREE(2),FOUR(3),FIVE(4),SIX(5),SEVEN(6),EIGHT(7),NINE(8),
    TEN(9),JACK(10),QUEEN(11),KING(12);
    int value;
    Faces(int v){
        value = v;
    }
    int getValue(){
        return value;
    }
}

public class Card
{

    private Faces face; // face of card ("Ace", "Deuce", ...)
    private String suit; // suit of card ("Hearts", "Diamonds", ...)
    private int value;
    // two-argument constructor initializes card's face and suit
    public Card( Faces cardFace, String cardSuit )
    {
        face = cardFace; // initialize face of card
        suit = cardSuit;
        value = face.getValue(); // initialize suit of card
    }
    // return String representation of Card
    @Override
    public String toString()
    {
        return face + " of " + suit;
    }
    public int get_value()
    {
        return value;
    }
} // end class Card
