package com.company;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
    int size;
    boolean[][] matrix;
    int matrixSize;
    int offset;

    public MyPanel(){
    }

    public MyPanel(int frameSize, boolean[][] mat, int numSq, int startOffset){
        size = frameSize-(2*startOffset);
        matrix = mat;
        matrixSize = numSq;
        offset = startOffset;
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int squSize = size/matrixSize;
        //g.clearRect(size,size,size,size);
        for (int i =0; i<matrixSize; i++){
            for (int j =0; j<matrixSize; j++){
                if (!matrix[i][j])
                    g.fillRect(i*squSize+1+offset,j*squSize+1+offset,squSize-1,squSize-1);
                else
                    g.clearRect(i*squSize+1+offset,j*squSize+1+offset,squSize-1,squSize-1);
            }
        }


    }
    public void setMatrix(boolean[][] mat){
        matrix = mat;
    }
}
