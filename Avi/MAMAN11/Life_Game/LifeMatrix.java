package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.Random;


/**
 *
 * @author avieren
 */
public class LifeMatrix {
    private boolean[][] matrix;
    private boolean[][] tempMatrix;
    JFrame frame;
    private MyPanel panel;

    public LifeMatrix(){
    }
    public LifeMatrix(int size){
        matrix = new boolean [size][size];
        Random randomNumbers = new Random();
        for (int i=0;i<size;i++){
            for (int j=0;j<size;j++){
                matrix[i][j]= randomNumbers.nextBoolean();
            }
        }
        tempMatrix = new boolean [size][size];
        frame = new JFrame();

    }

    public void calculate(){
        for (int i=0;i<matrix[0].length;i++){
            for (int j=0;j<matrix[1].length;j++){
                int neighbors = getNeighbors(matrix,i,j);
                tempMatrix[i][j]= nextGeneration(neighbors, matrix[i][j]);

            }
        }
        matrix = tempMatrix;
    }

    private int getNeighbors(boolean[][] matrix, int i, int j) {
        int startPosX = (i - 1 < 0) ? i : i-1;
        int startPosY = (j - 1 < 0) ? j : j-1;
        int endPosX =   (i + 1 > matrix[0].length-1) ? i : i+1;
        int endPosY =   (j + 1 > matrix[1].length-1) ? j : j+1;
        int cnt = 0;
        for (int row = startPosX;row <=endPosX;row++){
            for (int col = startPosY;col<=endPosY; col++){
                if ((row == i) & (col==j))
                    continue;
                if (matrix[row][col])
                    cnt++;
            }
        }
        return cnt;
    }

    private boolean nextGeneration(int neighbors, boolean state) {
        if ((neighbors>=4) | (neighbors<=1))
            return false;
        else if (neighbors==3 | state)
            return true;
        else
            return  false;
    }


    public void showMatrix(int size, int offset){
        panel = new MyPanel(size,matrix,matrix[0].length,offset);
        frame.setSize(size,size);
        frame.getContentPane().add(panel);
        frame.setLocationRelativeTo(null);
        frame.setBackground(Color.BLACK);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public boolean[][] getMatrix() {
        return matrix;
    }

    public void refreshPaint(int size, int offset) {
        panel.setMatrix(matrix);
        frame.getContentPane().repaint();
    }
}
