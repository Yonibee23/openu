package com.company;

import javax.swing.JOptionPane;
/**
 *
 * @author avieren
 */
public class ManageGame {
    private LifeMatrix matrix;

    public ManageGame(int size){
        matrix = new LifeMatrix(size);
    }

    public static void main(String[] args)
    {
        int size = 10;
        ManageGame game = new ManageGame(10);
        game.matrix.showMatrix(600,100);
        while (JOptionPane.showConfirmDialog(null, "Do you want move to next generation?") == 0) {
            game.matrix.calculate();
            game.matrix.refreshPaint(600, 100);
            //game.matrix.showMatrix(600);
        }
    System.exit(0);
    }


}