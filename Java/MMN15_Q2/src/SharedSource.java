import java.util.ArrayList;

class SharedSource {

    private ArrayList<int[]> sharedList = new ArrayList<>();

    // each element in arrayList contains 1d array
    SharedSource(int[] arr) {
        for (int value : arr) {
            sharedList.add(new int[]{value});
        }
    }

    // get element form sharedList
    synchronized int[] getElement() {
        int[] arr;
        if (!sharedList.isEmpty()){
            arr = sharedList.get(0);
            sharedList.remove(0);}
        else{
            //make sure that if sharedList is empty an ilegal value is entered.
            arr = new int[] {101};
        }
        return arr;
    }

    // push element to sharedList
    synchronized void pushElement(int[] arr) {
        sharedList.add(arr);
    }

    // returns sharedList length
    synchronized int size() {
        return sharedList.size();
    }

    @Override
    public String toString() {
        ArrayList<Integer> tmp = new ArrayList<>();
        for (int[] e : sharedList){
            for (int i : e){
                tmp.add(i);
            }
        }
        return tmp.toString();
    }
}
