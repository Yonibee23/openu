import java.util.concurrent.*;

class RunSort {

    private int arrSize, numThreads;
    private SharedSource mainSharedList;

    RunSort(int n, int m) {
        this.arrSize = n;
        this.numThreads = m;
        mainSharedList = new SharedSource(Utils.getRandomArr(arrSize));
    }

    void Sort() {
        System.out.println("Array before sort:");
        System.out.println(mainSharedList.toString());
        ExecutorService ex = Executors.newCachedThreadPool();
        while (mainSharedList.size() > 1) {
            for (int i = 0; i < numThreads; i++) {
                ex.execute(new Merge(mainSharedList));
            }
        }
        ex.shutdown();
        System.out.println("Array ater sort:");
        System.out.println(mainSharedList.toString());
    }
}
