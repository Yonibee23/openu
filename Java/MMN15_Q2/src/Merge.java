import java.util.ArrayList;
import java.util.Collections;

public class Merge implements Runnable {

    private SharedSource sharedLst;

    Merge(SharedSource arr) {
        this.sharedLst = arr;
    }

    @Override
    public void run() {

        ArrayList<Integer> tmpArr = new ArrayList<>();

        if (sharedLst.size() > 1) {
            int[] arr1 = sharedLst.getElement();
            int[] arr2 = sharedLst.getElement();


            // add primitive arr1 to arrayList
            for (int value : arr1) {
                tmpArr.add(value);
            }

            // add primitive arr2 to arrayList
            if (arr2[0] < 100) {
                for (int value : arr2) {
                    tmpArr.add(value);
                }
            }

            // sort the array list
            Collections.sort(tmpArr);

            // convert array list to primitive array back
            int[] mArr = new int[tmpArr.size()];
            for (int i = 0; i < tmpArr.size(); i++) {
                mArr[i] = tmpArr.get(i);
            }

            // push merged element back to sharedList
            sharedLst.pushElement(mArr);
        }
    }
}
