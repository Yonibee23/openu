import java.util.Random;

class Utils {

    //returns random array of size "arr_size"
    static int[] getRandomArr(int arr_size) {
        Random rn = new Random();
        int[] arr = new int[arr_size];

        for (int i = 0; i < arr_size; i++) {
            arr[i] = rn.nextInt(100) + 1;
        }
        return arr;
    }
}
