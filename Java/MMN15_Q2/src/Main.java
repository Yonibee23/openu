import java.util.Scanner;

public class Main {

    // main function to run tester
    public static void main(String[] args) {
        int n, m;

        Scanner inp = new Scanner(System.in);
        System.out.println("Enter desired array size");
        n = inp.nextInt();

        Scanner inp2 = new Scanner(System.in);
        System.out.println("Enter desired number of threads");
        m = inp.nextInt();

        RunSort rs = new RunSort(n, m);
        rs.Sort();
    }
}
