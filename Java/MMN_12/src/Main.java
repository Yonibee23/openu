public class Main {

    public static void main(String[] args) {
        Tester tester = new Tester();
        tester.testPlus();
        tester.testMinus();
        tester.testDerivative();
        tester.testEquals();
    }
}
