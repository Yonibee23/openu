class PolynomElement {

    private Integer power;
    private Double coefficient;

    //power getter.
    Integer getPower() {
        return power;
    }
    //returns power as string.
    String getPowerToStr() {
        return power.toString();
    }
    //coefficient getter.
    Double getCoefficient() {
        return coefficient;
    }
    //returns coefficient as string.
    String getCoefficientToStr() {
        return coefficient.toString();
    }
    //power setter.
    void setPower(Integer pow) {
        this.power = pow;
    }
    //coefficient setter.
    void setCoefficient(Double coef) {
        this.coefficient = coef;
    }
    //return sign operators per coefficient.
    String isPositiveToStr() {
        return coefficient >= 0 ? "+" : "-";
    }
}
