import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class PolynomBuilder {

    private List<Integer> powerList;
    private List<Double> coefficientList;
    private ArrayList<PolynomElement> polynom;

    PolynomBuilder(List<Double> coef, List<Integer> power) {
//        this.powerList = Arrays.asList(power);
        this.powerList = power;
//        this.coefficientList = Arrays.asList(coef);
        this.coefficientList = coef;
        this.polynom = new ArrayList<>();
    }

    //return current "max power value" index
    private int getMaxValueIndex() {
        int i = 0;
        while (powerList.get(i) == null) {
            i++;
        }
        Integer max = powerList.get(i);

        for (Integer val : powerList) {
            if (val == null) {
                continue;
            }
            max = Math.max(val, max);
        }

        return powerList.indexOf(max);
    }

    //update polynom element with values
    private void insertPolynomElement(int maxValueIndex) {
        PolynomElement tmpElem = new PolynomElement();
        tmpElem.setPower(powerList.get(maxValueIndex));
        tmpElem.setCoefficient(coefficientList.get(maxValueIndex));
        powerList.set(maxValueIndex, null);
        polynom.add(tmpElem);
    }

    //return built sorted polynom.
    ArrayList<PolynomElement> buildPolynom() {
        int index;
        for (int i = 0; i < powerList.size(); i++) {
            index = getMaxValueIndex();
            insertPolynomElement(index);
        }
        return polynom;
    }

}
    

