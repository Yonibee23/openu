import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Tester {

    private Polynom polyA;
    private Polynom polyB;

    Tester() {
        List<Double> arrayCoeficient1 = fillPolynomsCoefficient();
        List<Integer> arrayPower1 = fillPolynomsPower();

        List<Double> arrayCoeficient2 = fillPolynomsCoefficient();
        List<Integer> arrayPower2 = fillPolynomsPower();

        this.polyA = new Polynom(arrayCoeficient1, arrayPower1);
        this.polyB = new Polynom(arrayCoeficient2, arrayPower2);

    }
    //fill coefficient array from input
    private static List<Double> fillPolynomsCoefficient() {
        List<Double> tmp = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nInitiate polynom1 values\npress 'x' when you done with polynom1.");
        System.out.print("Insert Coefficient (Double): ");
        while (!scanner.hasNext("x")) {
            if (scanner.hasNextDouble()) {
                tmp.add(scanner.nextDouble());
            } else
                scanner.next();
        }
        return tmp;
    }
    //fill power array from input
    private static List<Integer> fillPolynomsPower() {
        List<Integer> tmp = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nInitiate polynom1 values\npress 'x' when you done with polynom1.");
        System.out.print("Insert Power (Integer): ");
        while (!scanner.hasNext("x")) {
            if (scanner.hasNextInt()) {
                tmp.add(scanner.nextInt());
            } else
                scanner.next();
        }
        return tmp;
    }
    //print out sum of Polynom1 + Polinom2
    void testPlus() {
        Polynom polyC = new Polynom(polyA.plus(polyB.getPolynom()));
        System.out.println("==================================");
        System.out.println("Polynom Plus TEST - Px1 + Px2");
        System.out.println("==================================");
        System.out.println(String.format("(%s)\n        +    \n(%s)\n", polyA.toString(), polyB.toString()));
        System.out.println(String.format("result:\n(%s)\n", polyC.toString()));

    }
    //print out sum of Polynom1 - Polinom2
    void testMinus() {
        Polynom polyC = new Polynom(polyA.minus(polyB.getPolynom()));
        System.out.println("==================================");
        System.out.println("Polynom Minus TEST Px1 - Px2");
        System.out.println("==================================");
        System.out.println(String.format("(%s)\n        -    \n(%s)\n", polyA.toString(), polyB.toString()));
        System.out.println(String.format("result:\n(%s)\n", polyC.toString()));
    }
    //print out Polynom1 derivative
    void testDerivative() {
        System.out.println("==================================");
        System.out.println("Polynom Derivative TEST - Px1' ");
        System.out.println("==================================");
        System.out.println("Original polynom:");
        System.out.println(String.format("(%s)\n", polyA.toString()));
        Polynom polyC = new Polynom(polyA.derivative());
        System.out.println("After derivative:");
        System.out.println(String.format("(%s)\n", polyC.toString()));
    }
    //print compare Polynoms - print out true if equals false if not.
    void testEquals() {
        System.out.println("==================================");
        System.out.println("Polynom equal TEST - Px1 = Px2 ?");
        System.out.println("==================================");
        boolean result = polyA.equals(polyB.getPolynom());
        System.out.println(String.format("(%s)\n  is equal to ??    \n(%s)\n", polyA.toString(), polyB.toString()));
        System.out.println(String.format("result:\n(%s)\n", result));
    }
}
