import java.util.ArrayList;
import java.util.List;

public class Polynom {

    private ArrayList<PolynomElement> polynom;

    Polynom(List<Double> coefArray, List<Integer> powArry) {
        PolynomBuilder a = new PolynomBuilder(coefArray, powArry);
        this.polynom = a.buildPolynom();
    }

    Polynom(ArrayList<PolynomElement> poly) {
        this.polynom = poly;
    }

    //returns polynom array.
    ArrayList<PolynomElement> getPolynom() {
        return polynom;
    }

    //return the sum of polynom1 + polynom2
    ArrayList<PolynomElement> plus(ArrayList<PolynomElement> polyToAdd) {

        boolean found = false;
        ArrayList<PolynomElement> tmp = new ArrayList<>();
        ArrayList<PolynomElement> PolyA = copyPolynom(polynom);
        ArrayList<PolynomElement> PolyB = copyPolynom(polyToAdd);

        for (PolynomElement element : PolyA) {
            for (PolynomElement element2 : PolyB) {
                if (element.getPower().equals(element2.getPower())) {
                    PolynomElement tmpElement = new PolynomElement();
                    tmpElement.setPower(element.getPower());
                    tmpElement.setCoefficient(element.getCoefficient() + element2.getCoefficient());
                    tmp.add(tmpElement);
                    PolyB.remove(element2);
                    found = true;
                    break;
                }
            }
            if (!found) {
                tmp.add(element);
            } else {
                found = false;
            }
        }
        for (PolynomElement elem : PolyB) {
            tmp.add(elem);
        }

        return tmp;


    }

    //return the sum of polynom1 - polynom2
    ArrayList<PolynomElement> minus(ArrayList<PolynomElement> polyToDecrease) {
        boolean found = false;
        ArrayList<PolynomElement> tmp = new ArrayList<>();
        ArrayList<PolynomElement> PolyA = copyPolynom(polynom);
        ArrayList<PolynomElement> PolyB = copyPolynom(polyToDecrease);

        for (PolynomElement element : PolyA) {
            for (PolynomElement element2 : PolyB) {
                if (element.getPower().equals(element2.getPower())) {
                    PolynomElement tmpElement = new PolynomElement();
                    tmpElement.setPower(element.getPower());
                    tmpElement.setCoefficient(element.getCoefficient() - element2.getCoefficient());
                    tmp.add(tmpElement);
                    PolyB.remove(element2);
                    found = true;
                    break;
                }
            }
            if (!found) {
                tmp.add(element);
            } else {
                found = false;
            }
        }
        for (PolynomElement elem : PolyB) {
            tmp.add(elem);
        }

        return tmp;
    }

    //return the polynom1 derivative
    ArrayList<PolynomElement> derivative() {
        ArrayList<PolynomElement> tmp = new ArrayList<>();
        for (PolynomElement element : polynom) {
            if (element.getPower() != 0) {
                PolynomElement tmpElement = new PolynomElement();
                tmpElement.setPower(element.getPower() - 1);
                tmpElement.setCoefficient(element.getPower().doubleValue() * element.getCoefficient());
                tmp.add(tmpElement);
            }
        }
        return tmp;
    }

    @Override
    public String toString() {
        String tmpStr = "Polynom: ";
        for (PolynomElement element : polynom) {
            if (element.getCoefficient() == 0.0) {
                continue;
            }
            tmpStr += (" " + element.isPositiveToStr() + element.getCoefficientToStr() + "^" + element.getPowerToStr());
        }
        return tmpStr;
    }

    //return true if polynoms are identical.
    boolean equals(ArrayList<PolynomElement> polyToCompare) {

        if (polynom.size() != polyToCompare.size()) {
            return false;
        }

        for (int i = 0; i < polynom.size(); i++) {
            if (!polynom.get(i).getPower().equals(polyToCompare.get(i).getPower())) {
                return false;
            }
            if (!polynom.get(i).getCoefficient().equals(polyToCompare.get(i).getCoefficient())) {
                return false;
            }
        }
        return true;
    }

    //duplicate polynom.
    private ArrayList<PolynomElement> copyPolynom(ArrayList<PolynomElement> poly) {
        ArrayList<PolynomElement> tmpArry = new ArrayList<>();
        tmpArry.addAll(poly);
        return tmpArry;
    }
}
