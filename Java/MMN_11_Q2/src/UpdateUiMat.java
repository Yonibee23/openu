import javax.swing.JPanel;
import java.awt.*;

public class UpdateUiMat extends JPanel {

    private int[][] matrix;

    //updates matrix to hold the latest life map.
    void updateMatrix(int[][] mtrx) {
        this.matrix = mtrx;
    }

    //paint the current generation life matrix
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        int SQUARE_DIM = 50;
        int MAT_DIM = 10;
        int cordX = 0, cordY = 0;

        for (int i = 0; i < MAT_DIM; i++) {
            for (int j = 0; j < MAT_DIM; j++) {


                if (matrix[i][j] == 1) {
                    g.setColor(Color.pink); //pink color represents life
                    g.fillRect(cordX, cordY, SQUARE_DIM, SQUARE_DIM);
                } else {
                    g.setColor(Color.gray); //gray color represents death
                }
                g.fillRect(cordX, cordY, SQUARE_DIM, SQUARE_DIM);
                g.setColor(Color.black);
                g.drawRect(cordX, cordY, SQUARE_DIM, SQUARE_DIM);
                cordY += SQUARE_DIM;
            }
            cordX += SQUARE_DIM;
            cordY = 0;
        }
    }
}
