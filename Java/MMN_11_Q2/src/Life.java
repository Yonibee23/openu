import java.util.Random;

class Life {

    private static int[][] matrix;
    private static int[][] nextGenMatrix;
    private final Integer matSize;

    Life(Integer matrixSize) {
        this.matSize = matrixSize;
    }

    void initiateMatrix() {
        matrix = new int[matSize][matSize];
        Random rand = new Random();

        for (int i = 0; i < matSize; i++) {
            for (int j = 0; j < matSize; j++) {
                matrix[i][j] = rand.nextInt(2);
            }
        }
    }

    void nextGenMap() {
        nextGenMatrix = new int[matSize][matSize];
        for (int i = 0; i < matSize; i++) {
            for (int j = 0; j < matSize; j++) {
                Site a = new Site(matrix, i, j);
                nextGenMatrix[i][j] = a.nextGeneration();
            }
        }
        matrix = nextGenMatrix;
    }

    int[][] getMat(boolean initMat) {
        if (initMat) {
            return matrix;
        }
        return nextGenMatrix;
    }

}