import javax.swing.*;

public class Main {

    public static void main(String[] args) {

        DrawUI dt = new DrawUI();
        dt.draw();

        while (JOptionPane.showConfirmDialog(null, "Pink = life\nGrey = death\n\nAnalyze next generation?") == 0) {
            dt.nextGen();
        }
        System.exit(0);
    }
}