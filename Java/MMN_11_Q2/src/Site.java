class Site {


    private final int[][] mtrx;
    private final int cordy;
    private final int cordx;


    Site(int[][] matrix, int x, int y) {

        this.mtrx = matrix;
        this.cordx = x;
        this.cordy = y;
    }

    //check if site is alive
    private boolean isAlive(int x, int y) {
        return mtrx[x][y] == 1;
    }

    //return number of neighbors
    private int countNeighbors() {
        int neighborCounter = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (cordx + i == cordx && cordy + j == cordy) {
                    continue;
                }
                try {
                    if (isAlive(cordx + i, cordy + j)) {
                        neighborCounter += 1;
                    }
                } catch (Exception ignored) {
                }
            }
        }
        return neighborCounter;
    }

    //return the next generation site state 0: death, 1: live
    int nextGeneration() {

        int neighbor = countNeighbors();
        int nextGen = 0;
        if (isAlive(cordx, cordy)) {
            if (neighbor > 1 && neighbor < 4) {
                nextGen = 1;
            }
        } else {
            if (neighbor == 3) {
                nextGen = 1;
            }
        }
        return nextGen;
    }
}