import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Path;

public class PhoneUI extends JFrame implements ActionListener {

    private OpenFileBrowser fileOpen = new OpenFileBrowser();
    private PhoneBook pb = new PhoneBook();
    private JPanel panel = new JPanel();
    private JButton addCon = new JButton("Add contact");
    private JButton updateCon = new JButton("Update contact");
    private JButton eraseCon = new JButton("Remove contact");
    private JButton searchCon = new JButton("search contact");
    private JButton loadBook = new JButton("load phonebook");
    private JButton saveBook = new JButton("save phonebook");
    private JTextArea contListTxt = new JTextArea(10, 20);

    PhoneUI() {
        super("PhoneBook");
        panel.setLayout(new GridLayout(2, 3));

        //add buttons ordered by grid
        panel.add(addCon);
        panel.add(updateCon);
        panel.add(eraseCon);
        panel.add(searchCon);
        panel.add(loadBook);
        panel.add(saveBook);

        //add action listener to all buttons
        addCon.addActionListener(this);
        updateCon.addActionListener(this);
        eraseCon.addActionListener(this);
        searchCon.addActionListener(this);
        loadBook.addActionListener(this);
        saveBook.addActionListener(this);

        add(panel, BorderLayout.SOUTH);

        //add scroll bar to txt area
        add(new JScrollPane(contListTxt), BorderLayout.CENTER);
        setSize(600, 400);
        setVisible(true);
    }

    // all keys policy are managed in the function below.
    @Override
    public void actionPerformed(ActionEvent e) {
        //add new contact
        if (e.getSource() == addCon) {
            String name = JOptionPane.showInputDialog("enter: contact name");
            String number = JOptionPane.showInputDialog("enter: contact phone number");
            try {
                PersonDetails cont = new PersonDetails(name, number);
                pb.addContact(cont);
                contListTxt.setText(pb.toString());
            }
            catch (Exception a){
                JOptionPane.showMessageDialog(null, "No input - opperation was canceled by user");
            }
        }

        //update contact phone number
        else if (e.getSource() == updateCon) {
            String name = JOptionPane.showInputDialog("enter: contact name");
            String number = JOptionPane.showInputDialog("enter: contact phone number");
            PersonDetails cont = new PersonDetails(name, number);
            pb.updateContact(cont);
            contListTxt.setText(pb.toString());
        }

        //erase contact from phone book
        else if (e.getSource() == eraseCon) {
            String details = JOptionPane.showInputDialog("enter: contact name");
            PersonDetails cont = new PersonDetails(details, "");
            pb.removeContact(cont);
            contListTxt.setText(pb.toString());

            //search for phone number by a given name
        } else if (e.getSource() == searchCon) {
            String result;
            String details = JOptionPane.showInputDialog("enter: contact name");
            PersonDetails cont = new PersonDetails(details, "");
            result = pb.searchContact(cont);
            if (!result.equals("")) {
                JOptionPane.showMessageDialog(null, result);
            } else {
                JOptionPane.showMessageDialog(null, "not found!");
            }

            //load workbook from tct file
        } else if (e.getSource() == loadBook) {
            Path path = fileOpen.choosFilePath();

            try {
                pb.loadPhoneBook(path);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            contListTxt.setText(pb.toString());

            //save phone book to txt file
        } else if (e.getSource() == saveBook) {
            Path spath = fileOpen.choosFilePath();
            try {
                pb.savePhoneBook(spath);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }


    }
}

