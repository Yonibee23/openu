import javax.swing.*;
import java.io.*;
import java.nio.file.Path;
import java.util.TreeMap;
import java.util.Set;

public class PhoneBook {

    private TreeMap<String, String> pbook;

    PhoneBook() {
        pbook = new TreeMap<>();
    }

    @Override
    public String toString() {
        String pbStr = "";
        Set<String> keys = pbook.keySet();
        for (String key : keys) {
            pbStr = pbStr.concat(key + ": " + pbook.get(key) + "\n");
        }
        return pbStr;
    }

    //true if contact exists in treemap else false
    private boolean is_exist(PersonDetails p) {
        boolean is_exist = true;
        try {
            if (!pbook.containsKey(p.getName())) {
                JOptionPane.showMessageDialog(null, "contact: " + p.getName() + "  is not exist!");
                is_exist = false;
            }
        } catch (Exception e) {
            return false;
        }
        return is_exist;
    }

    //add contact details to treeMap
    void addContact(PersonDetails p) {
        if (!pbook.containsKey(p.getName())) {
            pbook.put(p.getName(), p.getNumber());
        }
    }

    //remove contact from treeMap
    void removeContact(PersonDetails p) {
        if (is_exist(p)) {
            pbook.remove(p.getName());
        }
    }

    //update contact phone in treeMap
    void updateContact(PersonDetails p) {
        if (is_exist(p)) {
            pbook.remove(p.getName());
            pbook.put(p.getName(), p.getNumber());
        }
    }

    //search for contact in treeMap
    String searchContact(PersonDetails p) {
        if (is_exist(p)) {
            return "" + pbook.get(p.getName()) + " " + pbook.get(p.getNumber());
        }
        return "";
    }

    //load phone book from txt file and add it to phone book
    void loadPhoneBook(Path path) throws IOException {
        String st;
        BufferedReader br = new BufferedReader(new FileReader(String.valueOf(path)));
        while ((st = br.readLine()) != null) {
            String[] arr = st.split(" ");
            addContact(new PersonDetails(arr[0], arr[1]));
        }
    }

    //save phone book from UI and save it to txt file
    void savePhoneBook(Path path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(String.valueOf(path)));

        try {
            Set<String> keys = pbook.keySet();
            for (String key : keys) {
                writer.write(key + " " + pbook.get(key) + "\n");
            }
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}

