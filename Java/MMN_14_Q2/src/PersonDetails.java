class PersonDetails {

    private String personName;
    private String phoneNumber;

    //holds a contact details
    PersonDetails(String name, String number) {

        this.personName = name;
        this.phoneNumber = number;
    }

    //get person name
    String getName() {
        return personName;
    }

    //get person phon number
    String getNumber() {
        return phoneNumber;
    }


}
