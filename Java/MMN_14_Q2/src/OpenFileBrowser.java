import javax.swing.*;
import java.awt.*;
import java.nio.file.Path;

//open file browser uses to save / load files
class OpenFileBrowser extends Component {

    private JFileChooser chooser = new JFileChooser();
    //open file browser.
    Path choosFilePath(){
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int res = chooser.showOpenDialog(this);
        if(res == JFileChooser.CANCEL_OPTION) System.exit(1);
        return chooser.getSelectedFile().toPath();
    }

}
