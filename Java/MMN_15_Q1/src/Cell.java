class Cell {

    private Integer x, y;
    private int[][] matrix;

    Cell(int[][] mat, Integer cordX, Integer cordY) {
        this.x = cordX;
        this.y = cordY;
        this.matrix = mat;
    }

    private boolean isWhiteColor(int xCord, int yCord) {
        return matrix[xCord][yCord] == 0;
    }

    boolean isOwnWhiteColor() {
        return matrix[x][y] == 0;
    }

    void setColor(int color) {
        matrix[x][y] = color > 0 ? 1 : 0;
    }

    // get cell color after a cut base on his neighbor
    synchronized int getNextColor() {

        // cell is white no need to get further.
        if (isWhiteColor(x, y)) {
            return 0;
        } else {
            for (int i = -1; i < 2; i++) {
                for (int j = -1; j < 2; j++) {

                    try {
                        if (x + i == x && y + j == y) {
                            continue;
                        }
                        // means that pixel is black but has a white neighbor.
                        if (isWhiteColor(x + i, y + j)) {
                            return 0;
                        }
                    } catch (Exception ignored) {
                    }
                }
            }
            // means that pixel is black and all its neighbors are black.
            return 1;
        }
    }
}
