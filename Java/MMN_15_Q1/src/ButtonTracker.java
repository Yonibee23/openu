import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// creates a button panel + set policy to buttons.
public class ButtonTracker implements ActionListener {

    private int numThreads, rounds;
    private Board board;
    private Matrix mtrx;
    private JPanel btnPanel;
    private JButton goBtn;
    private JButton clearBtn;

    ButtonTracker(Matrix m, Board brd, int trds, int rnds) {
        this.mtrx = m;
        this.board = brd;                          // Board instance
        this.numThreads = trds;                    // number of threads
        this.rounds = rnds;                        // number of rounds
        btnPanel = new JPanel();                   // create JPanel
        goBtn = new JButton("GO!");           // create GO button
        clearBtn = new JButton("Clear");      // create clear button
        btnPanel.add(goBtn, BorderLayout.WEST);    // add button to panel
        btnPanel.add(clearBtn, BorderLayout.EAST); // add button2 to panel
        goBtn.addActionListener(this);          // Go button listener
        clearBtn.addActionListener(this);       // Clear button listener
    }

    // return JPanel object
    JPanel getBtnPanel() {
        return btnPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        int numOfLines = mtrx.getMatSize() / numThreads;  // number of lines to take care of in each thread

        // clear button zero the matrix and the display.
        if (e.getSource() == clearBtn) {
            mtrx.initiateMatrix();
            board.setMatrix(mtrx);

            // go button starts the shrinking process.
        } else if (e.getSource() == goBtn) {
            Matrix newMat = new Matrix(mtrx.getMatSize());                        // new matrix for results.
            ExecutorService ex = Executors.newCachedThreadPool();                 // executor instance
            for (int i = 0; i < rounds; i++) {                                    // number of cuts
                newMat.initiateMatrix();                                          // zero new matrix
                int startLine = 0;
                for (int j = 0; j < numThreads; j++) {                            // number of threads per cuts
                    ex.execute(new Cutter(mtrx, newMat, numOfLines, startLine));  // creates threads
                    startLine += numOfLines;
                }
                try {  // wait intil process are done or timeout
                    ex.awaitTermination(500, TimeUnit.MILLISECONDS);
                } catch (InterruptedException exc) {
                    exc.printStackTrace();
                }
                board.setMatrix(newMat); // set updated matrix after cut to repaint board.
                mtrx = newMat.cloneMat(); // save the last status to the main matrix.
            }
            ex.shutdown(); // close executor.
        }
    }
}

