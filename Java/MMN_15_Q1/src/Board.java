import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Board extends JPanel {

    private Matrix mtrx;
    private int matSize;

    Board(Matrix matrix) {
        this.mtrx = matrix;
        this.matSize = matrix.getMatSize();
        mtrx.initiateMatrix();
    }

    // matrix setter
    void setMatrix(Matrix m) {
        mtrx = m.cloneMat();
        refrash();
    }

    // repaint the matrix on panel
    private void refrash() {
        repaint();

    }

    // paint the Matrix white = 0, grey = 1
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int SQUARE_DIM = 30;
        int MAT_DIM = matSize;
        int cordX = 0, cordY = 0;
        for (int i = 0; i < MAT_DIM; i++) {
            for (int j = 0; j < MAT_DIM; j++) {
                if (mtrx.isWhite(i, j)) {
                    g.setColor(Color.white); //set white color to rect
                    g.fillRect(cordX, cordY, SQUARE_DIM, SQUARE_DIM);
                } else {
                    g.setColor(Color.gray); //set gray color to rect
                }
                g.fillRect(cordX, cordY, SQUARE_DIM, SQUARE_DIM); //draw rect itself
                g.setColor(Color.black);
                g.drawRect(cordX, cordY, SQUARE_DIM, SQUARE_DIM); //draw black grid around matrix
                cordY += SQUARE_DIM;
            }
            cordX += SQUARE_DIM;
            cordY = 0;
        }
    }
}
