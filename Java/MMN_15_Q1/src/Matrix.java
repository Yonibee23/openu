class Matrix {

    private int[][] matrx;
    private int matSize;

    Matrix(int mSize) {
        this.matSize = mSize;
        matrx = new int[mSize][mSize];
    }

    // initiate matrix with zeros
    void initiateMatrix() {
        for (int i = 0; i < matSize; i++) {
            for (int j = 0; j < matSize; j++) {
                matrx[i][j] = 0;
            }
        }
    }

    // set matrix color 0 = white, 1 = black
    void setCellColor(int x, int y, int color) {
        matrx[x][y] = color > 0 ? 1 : 0;
    }

    boolean isWhite(int x, int y) {
        return matrx[x][y] == 0;
    }

    int getMatSize() {
        return matSize;
    }

    Matrix cloneMat() {
        Matrix cln = new Matrix(matSize);
        for (int i = 0; i < matSize; i++) {
            for (int j = 0; j < matSize; j++) {
                cln.setCellColor(i,j,matrx[i][j]);
            }
        }
        return cln;
    }

    // return matrix
    int[][] getMatrx() {
        return matrx;
    }

    // print matrix debug only
    void printMat() {
        for (int i = 0; i < matSize; i++) {
            System.out.print("|");
            for (int j = 0; j < matSize; j++) {
                System.out.print(" " + matrx[i][j] + " ");
            }
            System.out.println("|");
        }
        System.out.println();
    }
}
