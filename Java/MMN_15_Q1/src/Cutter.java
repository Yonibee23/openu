public class Cutter implements Runnable {

    private Matrix mtrx;
    private Matrix newMtrx;
    private int numOfLines, startLine, matSize;

    Cutter(Matrix matrix, Matrix nMatrix, int nLines, int sLine){
        this.mtrx = matrix;
        this.newMtrx = nMatrix;
        this.startLine = sLine;
        this.numOfLines = nLines;
        this.matSize = matrix.getMatSize();
    }

    // set the right color for each cell.
    @Override
    public void run() {
        int line = startLine;
        for (int i=0; i < numOfLines; i++){
            for (int j=0; j < matSize; j++){
                Cell cell = new Cell(mtrx.getMatrx(), line, j);
                newMtrx.setCellColor(line, j, cell.getNextColor());
            }
            line++;
        }
    }
}
