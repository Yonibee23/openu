// Fig. 14.29: MouseTrackerFrame.java
// Testing MouseTrackerFrame.
import javax.swing.*;

public class MainRun
{
   public static void main( String[] args )
   {
      int mSize = Integer.parseInt(JOptionPane.showInputDialog("enter mat size NxN"));
      int tSize = Integer.parseInt(JOptionPane.showInputDialog("enter number of threads"));
      int rSize = Integer.parseInt(JOptionPane.showInputDialog("enter number of cuts"));

      MouseTrackerFrame mouseTrackerFrame = new MouseTrackerFrame(mSize, tSize, rSize);
      mouseTrackerFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      mouseTrackerFrame.setSize( 400, 400 ); // set frame size
      mouseTrackerFrame.setVisible( true ); // display frame
   } // end main
}
