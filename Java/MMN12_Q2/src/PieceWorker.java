public class PieceWorker extends  Employee {

    private int salesItems; 
    private double paymentPerItem; 
    
    PieceWorker(String first, String last, String ssn, int day, int month, int year,
                int sales, double payment)
    {
        super( first, last, ssn, year, day, month );
        setSalesItems( sales );
        setPayRate( payment );
    } 

    // set PayRate rate
    private void setPayRate(double payment)
    {
        assert (payment > 0.0) : "Illegal payment [payment must be: payment > 0]";
        paymentPerItem = payment;
       
    } // end method setPayRate

    // return PaymentPerItem rate
    private double getPaymentPerItem()
    {
        return paymentPerItem;
    } // end method getPaymentPerItem

    // set Sales Items amount
    private void setSalesItems(int sales)
    {
        assert (sales > 0.0) : "Illegal sales [sales must be: price > 0]";
        salesItems = sales;
    } // end method setSalesItems

    // return SalesItems amount
    private int getSalesItem()
    {
        return salesItems;
    } // end method getGrossSales

    // calculate earnings; override abstract method earnings in Employee
    @Override
    public double earnings()
    {
        return getSalesItem() * getPaymentPerItem();
    }


    @Override
    public String toString()
    {
        return String.format( "%s: %s\n%s: %d; %s: %f\n%s: $%.2f",
                "piece worker", super.toString(),
                "sales amount", getSalesItem(),
                "payment per item", getPaymentPerItem(), "earnings", earnings() );
    }
}
