import java.util.ArrayList;
import java.util.Scanner;

class EmployeeDetails {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;
    private int day;
    private int year;
    private int month;
    private int empType;
    private ArrayList<Employee> empList;
    private Scanner scanner = new Scanner(System.in);

    EmployeeDetails(int typ, ArrayList<Employee> employeeArrayList) {
        this.empType = typ;
        this.empList = employeeArrayList;
    }

    private void setPersonalDetails(){

        System.out.println("Enter first name:");
        firstName = scanner.nextLine();
        System.out.println("Enter last name:");
        lastName = scanner.nextLine();
        System.out.println("Enter ssn:");
        socialSecurityNumber = scanner.nextLine();
        System.out.println("Enter day of birth:");
        day = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter month of birth:");
        month = Integer.parseInt(scanner.nextLine());
        System.out.println("Enter year of birth:");
        year = Integer.parseInt(scanner.nextLine());
    }


    private void setHourEmp() {
        this.setPersonalDetails();
        System.out.print("Enter wage per hour");
        int wage = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter work hours per week");
        int hours = Integer.parseInt(scanner.nextLine());
        empList.add(new HourlyEmployee(firstName, lastName, socialSecurityNumber, day, month, year, wage, hours));
    }

    private void setCommissionEmp() {
        this.setPersonalDetails();
        System.out.print("Enter gross sales");
        double grosSales = Double.parseDouble(scanner.nextLine());
        System.out.print("Enter commission rate");
        double comRate = Double.parseDouble(scanner.nextLine());
        empList.add(new CommissionEmployee(firstName, lastName, socialSecurityNumber, day, month, year, grosSales, comRate));
    }

    private void setBasePlusCommisionEmp() {
        this.setPersonalDetails();
        System.out.print("Enter gross sales");
        double grosSales = Double.parseDouble(scanner.nextLine());
        System.out.print("Enter commission rate");
        double comRate = Double.parseDouble(scanner.nextLine());
        System.out.print("Enter salary per week");
        double salary = Double.parseDouble(scanner.nextLine());
        empList.add(new BasePlusCommissionEmployee(firstName, lastName, socialSecurityNumber, day, month, year, grosSales, comRate, salary));
    }

    private void setSalariedEmp() {
        this.setPersonalDetails();
        System.out.print("Enter salary per week");
        double salary = Double.parseDouble(scanner.nextLine());
        empList.add(new SalariedEmployee(firstName, lastName, socialSecurityNumber, day, month, year, salary));
    }

    private void setPieceEmp() {
        this.setPersonalDetails();
        System.out.print("Enter number of items");
        int items = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter payments per item");
        double payment = Double.parseDouble(scanner.nextLine());
        empList.add(new PieceWorker(firstName, lastName, socialSecurityNumber, day, month, year, items, payment));
    }

    void returnEmployee() {
        if (empType == 1) {this.setHourEmp();}
        else if (empType == 2){this.setCommissionEmp();}
        else if (empType == 3){this.setBasePlusCommisionEmp();}
        else if (empType == 4){this.setSalariedEmp();}
        else if (empType == 5){this.setPieceEmp();}
        else throw new IllegalArgumentException("Illegal employee type must be between 1-5" );
    }


}
