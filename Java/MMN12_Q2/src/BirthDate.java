import java.util.Calendar;

class BirthDate {

    private int birthDay;
    private int birthMonth;
    private int birthYear;

    BirthDate(int day, int month, int year) {
        setDay(day);
        setMonth(month);
        setYear(year);

    }

    private void setDay(int day) {
        assert (1 <= day && day <= 31) : "Illegal day [must be  0 < day < 32]";
        birthDay = day;
    }

    private void setMonth(int month) {
        assert (1 <= month && month <= 12) : "Illegal month [must be  0 < day < 13]";
        birthMonth = month;
    }

    private void setYear(int year) {
        assert (1918 <= year && year <= 2018) : "Illegal year [must be  1917 < day < 2019]";
        birthYear = year;
    }

    int getBirthDay() {
        return birthDay;
    }

    int getBirthMonth() {
        return birthMonth;
    }

    int getBirthYear() {
        return birthYear;
    }

    boolean isBirthInCurrentMonth() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.MONTH) + 1 == birthMonth;
    }
}
