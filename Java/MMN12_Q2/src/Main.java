import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {

        int numOfEmployees;
        ArrayList<Employee> empList = new ArrayList<>();

        System.out.println("Enter number of employees:");
        Scanner scanner = new Scanner(System.in);
        numOfEmployees = Integer.parseInt(scanner.nextLine());

        for (int i=0; i < numOfEmployees; i++){
            System.out.println("Employee type:\n1 - Hourly\n2 - Commission\n3 - BasePlusCommision\n4 - Salaried\n5 - Piece worker\n\n");
            int typ = Integer.parseInt(scanner.nextLine());
            EmployeeDetails tmpEmp = new EmployeeDetails(typ, empList);
            tmpEmp.returnEmployee();
        }

        for (int i=0; i < numOfEmployees; i++){
            String spc = "\n=========================\n";
            System.out.println(spc + empList.get(i));
        }


    }

}
