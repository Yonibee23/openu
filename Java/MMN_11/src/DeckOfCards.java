import java.util.ArrayList;
import java.util.Collections;

class DeckOfCards {

    private ArrayList<Card> deck = new ArrayList<>();
    private int currentCard = 0;
    private static final int NUMBER_OF_CARDS = 52;
    private static final int HALF_DECK = 26;

    DeckOfCards() {
        Integer[] faces = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};

        for (int count = 0; count < NUMBER_OF_CARDS; count++) {
            deck.add(new Card(faces[count % 13], suits[count / 13]));
        }
        Collections.shuffle(deck);
    }

    private Card dealCard() {
        if (currentCard < deck.size())
            return deck.get(currentCard++);
        else
            return null;
    }

    ArrayList<Card> dealHalfDeck() {
        ArrayList<Card> tmpPleyer = new ArrayList<>();
        for (int count = 0; count < HALF_DECK; count++) {
            tmpPleyer.add(this.dealCard());
        }
        return tmpPleyer;
    }
}
