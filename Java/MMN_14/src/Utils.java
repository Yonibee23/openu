import java.util.Random;

class Utils {

    //return random sets of size 10
    static Integer[] getRandomArr() {
        int size = 10;
        Random rn = new Random();
        Integer[] arr = new Integer[size];

        for (int i = 0; i < size; i++) {
            arr[i] = rn.nextInt(100) + 1;
        }
        return arr;
    }
}
