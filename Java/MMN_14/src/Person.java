public class Person implements Comparable<Person> {

    private String firstName;
    private String lastName;
    private String id;
    private String birth;

    public Person() {
    }

    Person(String fName, String lastName, String nId, String nBirth) {
        setFirstName(fName);
        setLastName(lastName);
        setId(nId);
        setBirth(nBirth);
    }

    //print person details
    void printDetails() {

        String details = "Person details:\nFirst name: " + this.firstName + "\nLast name : " + this.lastName + "\nid: " + this.id + "\n\"birthday  :" + this.birth;
        System.out.println(details);
    }

    //comparison function lexicography
    public int compareTo(Person somePerson) {

        if (firstName.compareTo(somePerson.firstName) > 0) {
            return 1;
        } else if (firstName.compareTo(somePerson.firstName) < 0) {
            return -1;
        } else if (lastName.compareTo(somePerson.lastName) < 0) {
            return -1;
        } else if (lastName.compareTo(somePerson.lastName) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    //returns first name
    private String getFirstName() {
        return firstName;
    }

    //return last name
    private String getlastName() {
        return lastName;
    }

    //returns id
    private String getId() {
        return id;
    }

    //returns birth date
    private String getBirth() {
        return birth;
    }

    //first name setter
    private void setFirstName(String name) {
        firstName = name;
    }

    //last name setter
    private void setLastName(String name) {
        lastName = name;
    }

    //id number setter
    private void setId(String idNum) {
        id = idNum;
    }

    //birthday setter
    private void setBirth(String brt) {
        birth = brt;
    }
}
