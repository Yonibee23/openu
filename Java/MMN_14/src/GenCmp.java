import java.util.ArrayList;

class GenCmp {
    //retrieve minimum object in set
    static <T extends Comparable<T>> T minimum(ArrayList<T> arr) {

        T min = arr.get(0);

        for (T i : arr) {
            if (min.compareTo(i) > 0) {
                min = i;
            }
        }
        return min;
    }

}
