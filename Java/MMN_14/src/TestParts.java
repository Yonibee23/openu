import java.util.ArrayList;
import java.util.Scanner;

// this class will manage all of the test steps will run part by part as described in the assignment
class TestParts {

    private Integer[] arr1 = Utils.getRandomArr();
    private Integer[] arr2 = Utils.getRandomArr();
    private Integer[] arr3 = Utils.getRandomArr();
    private Integer[] arr4 = new Integer[2];

    private GenSet<Integer> gs1 = new GenSet<>(arr1);
    private GenSet<Integer> gs2 = new GenSet<>(arr2);
    private GenSet<Integer> gs3 = new GenSet<>(arr3);
    private Scanner inp = new Scanner(System.in);

    //runs all programs steps
    void runParts() {
        partOne();
        partTwo();
        partThree();
        partFour();
        partFive();
        partSix();
    }

    //part one random unique sets
    private void partOne() {
        System.out.println("\n");
        System.out.println("Part1: 3 random sets without duplicates values");
        System.out.println("random set1: " + gs1.toString());
        System.out.println("random set2: " + gs2.toString());
        System.out.println("random set3: " + gs3.toString());
        System.out.println("============================================\n");
    }

    //union between set1 and set2
    private void partTwo() {
        System.out.println("Part2: Union between set1 to set2");
        System.out.println("set1: " + gs1.toString());
        System.out.println("set2: " + gs2.toString());
        gs1.union(gs2.iterator());
        System.out.println("\nset1 union set2: -> \nset1 = " + gs1.toString());
        System.out.println("============================================\n");
    }

    //intersect set1 to set3
    private void partThree() {
        System.out.println("Part3: intersect between set1 to set3");
        System.out.println("set1: " + gs1.toString());
        System.out.println("set3: " + gs3.toString());
        gs1.intersect(gs3.iterator());
        System.out.println("\nset1 intersect set3: -> \nset1 = " + gs1.toString());
        System.out.println("============================================\n");
    }

    //read 2 values to set4
    private void partFour() {
        System.out.println("Part4: read 2 values to set4 check if sub set with others");
        Scanner inp = new Scanner(System.in);
        System.out.println("enter an integer to set4");
        arr4[0] = inp.nextInt();
        System.out.println("enter an integer to set4");
        arr4[1] = inp.nextInt();

        GenSet<Integer> gs4 = new GenSet<>(arr4);
        System.out.println("\n");
        System.out.println("set1: " + gs1.toString());
        System.out.println("set2: " + gs2.toString());
        System.out.println("set3: " + gs3.toString());
        System.out.println("set4: " + gs4.toString());
        System.out.println("\n");
        System.out.println("set4 is sub set of set1? " + gs1.isSubset(gs4.iterator()));
        System.out.println("set4 is sub set of set2? " + gs2.isSubset(gs4.iterator()));
        System.out.println("set4 is sub set of set3? " + gs3.isSubset(gs4.iterator()));
        System.out.println("============================================\n");
    }

    //read integer check is number / add number / remove number
    private void partFive() {

        System.out.println("Part5: read integer - check is member \\ add \\ remove");
        System.out.println("enter one integer:");
        Integer num = inp.nextInt();
        System.out.println("\n");
        System.out.println("number: " + num + " is  a member of set1? " + gs1.isMember(num));

        gs2.insert(num);
        System.out.println("append number: " + num + " to set2: " + gs2.toString());

        gs3.delete(num);
        System.out.println("remove number: " + num + " from set3: " + gs3.toString());
        System.out.println("============================================\n");
    }

    //set 5 persons detail make lexicography sort
    private void partSix() {

        Person p1 = new Person("A", "A", "11111111", "01-01-200");
        Person p2 = new Person("B", "B", "22222222", "02-01-200");
        Person p3 = new Person("C", "C", "33333333", "03-01-200");
        Person p4 = new Person("D", "D", "44444444", "04-01-200");
        Person p5 = new Person("E", "E", "55555555", "05-01-200");

        ArrayList<Person> persons = new ArrayList<>();
        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
        persons.add(p4);
        persons.add(p5);

        Person p = GenCmp.minimum(persons);
        p.printDetails();

    }


}
