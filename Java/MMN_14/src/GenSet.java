import java.util.ArrayList;
import java.util.Arrays;

public class GenSet<E> {

    private ArrayList<E> set_list;

    //gen set constructor
    public GenSet() {
        set_list = new ArrayList<>();
    }

    GenSet(E[] arr) {
        set_list = new ArrayList<>(Arrays.asList(arr));
        removeDuplicate();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (E i : set_list) {
            s.append(i).append(", ");
        }
        return s.toString();
    }

    //filter duplicate elements
    private void removeDuplicate() {
        ArrayList<E> tmp = new ArrayList<>();
        for (E i : set_list) {
            if (!tmp.contains(i)) tmp.add(i);
        }
        set_list = tmp;
    }

    //union elements
    void union(ArrayList<E> arr) {
        ArrayList<E> tmp = new ArrayList<>();
        for (E i : arr) {
            if (!set_list.contains(i)) tmp.add(i);
        }
        set_list.addAll(tmp);
    }

    //intersect elements
    void intersect(ArrayList<E> arr) {
        ArrayList<E> tmp = new ArrayList<>();
        for (E i : arr) {
            if (set_list.contains(i)) tmp.add(i);
        }
        set_list = tmp;
    }

    //is one set is a subset of another set
    boolean isSubset(ArrayList<E> arr) {
        boolean isSub = true;
        for (E i : arr) {
            if (!set_list.contains(i)) {
                isSub = false;
                break;
            }
        }
        return isSub;
    }

    //is element is a member in the set
    boolean isMember(E element) {
        boolean isMem = true;
        if (!set_list.contains(element)) {
            isMem = false;
        }
        return isMem;
    }

    //insert element to set
    void insert(E element) {
        if (!set_list.contains(element)) set_list.add(element);
    }

    //remove element from set
    void delete(E element) {
        int ind;
        if (set_list.contains(element)) {
            ind = set_list.indexOf(element);
            set_list.remove(ind);
        }
    }

    //return iterator of self
    ArrayList<E> iterator() {
        return set_list;
    }

}
