package com.company;

public class Organ {

    private Double coefficient;
    private Integer exponent;

    public Organ(Double coefficient, Integer exponent){
        this.coefficient = coefficient;
        this.exponent = exponent;
    }

    @Override
    public String toString(){

        if (coefficient == 0)
            return "0";

        if (exponent == 0)
            return coefficient.toString();

        if (exponent == 1)
            return coefficient + "x";

        return coefficient + "x^" + exponent + " ";
    }
}
