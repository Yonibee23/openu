package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {


        try {
            Polynomial polyA = readPolynomialFromUser();
            System.out.println(polyA);

//            Polynomial polyB = readPolynomialFromUser();
//            System.out.println(polyB);

        }
        catch (Exception e){
            System.out.println(e);
        }


    }
    //STATICCCC MATHOD
    public static Polynomial readPolynomialFromUser() throws Exception {

        ArrayList coefficients = new ArrayList<Double>();
        ArrayList exponents = new ArrayList<Integer>();
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please insert exponent and coeff");

        while (keyboard.hasNextDouble() && keyboard.hasNextInt()) {
            System.out.println("Please insert exponent and coeff");

            coefficients.add(keyboard.nextDouble());
            exponents.add(keyboard.nextInt());
        }

        keyboard.close();

        return new Polynomial(coefficients, exponents);
    }
}
